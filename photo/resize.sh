#!/bin/bash
c=$(find . -maxdepth 1 -name "*.JPG" -print0 | tr -d -c "\000" | wc -c)

if [ $c = 0 ]; then
echo "No JPG files found."

else
START=$(date +%s)
echo "Now renaming" $c "JPG files."
exiftool '-FileName<DateTimeOriginal' -d "IMG%m%d%H%M%S.JPG" *.JPG

echo "Done. Now saving 1600px copies to /1600px."
for i in *.JPG
	do
		convert $i -resize 1600 1600px/$(basename $i .JPG)_1600px.jpg
		mv $i "`basename $i .JPG`.jpg"
		echo $i
	done

END=$(date +%s)
DIFF=$(( $END - $START ))
echo -e "\033[1mFinished in $DIFF seconds!"
fi
